package clases;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.Collections;

/**
 * 
 * Clase FicheroSecuencial, hereda de la clase GestorMonumentos. Contiene
 * m�todos para trabajar con ficheros secuenciales.
 * 
 * @author Klara Galuskova, 1� DAW
 *
 */

public class FicheroSecuencial extends GestorMonumentos {

	private static final String AVISO = "AVISO: SOLO SE TIENE EN CUENTA LA �LTIMA LISTA CON MONUMENTOS";
	private static final String LISTA_SIN_MONUMENTOS = "NO HAY MONUMENTOS EN LA LISTA";

	private String nombreFichero;
	private File fichero;

	public FicheroSecuencial(String nombreFichero) {
		super();
		this.nombreFichero = nombreFichero;
		this.fichero = new File(nombreFichero);
	}

	public String getNombreFichero() {
		return nombreFichero;
	}

	public void setNombreFichero(String nombreFichero) {
		this.nombreFichero = nombreFichero;
	}

	public File getFichero() {
		return fichero;
	}

	public void setFichero(File fichero) {
		this.fichero = fichero;
	}

	/**
	 * M�todo para dar de alta a monumentos. Llama al m�todo con el mismo nombre de
	 * la clase padre.
	 */
	public void altaMonumentos() {

		super.altaMonumentos(this.nombreFichero);

	}

	/**
	 * M�todo que no devuelve nada y no recibe nada. Permite crear el fichero
	 * secuencial, y crear su contenido recorriendo su atributo
	 * {@code ArrayList<Monumento> listaMonumentos}. Cada vez que se llama al
	 * m�todo, a�ade otro p�rrafo al fichero secuencial, con todos los monumentos de
	 * este atributo, y la fecha de actualizaci�n.
	 */
	public void crearFichero() {

		PrintWriter escritor = null;

		try {

			LocalTime horaActual = LocalTime.now();

			escritor = new PrintWriter(new FileWriter(this.nombreFichero, true));

			String linea = "FECHA DE ACTUALIZACI�N: "
					+ LocalDate.now().format(DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM)) + " "
					+ horaActual.getHour() + ":" + horaActual.getMinute();

			escritor.println(linea);

			for (Monumento monumento : this.listaMonumentos) {

				linea = monumento.toString();
				escritor.println(linea);

			}

			escritor.println();

		} catch (IOException e) {

			System.out.println(MENSAJE_IOEXCEPTION);

		} finally {

			if (escritor != null) {
				escritor.close();
			}

		}

	}

	/**
	 * M�todo que no devuelve nada y no recibe nada. Permite visualizar el contenido
	 * del fichero secuencial.
	 */
	public void mostrarFichero() {

		BufferedReader lector = null;

		try {

			lector = new BufferedReader(new FileReader(this.nombreFichero));

			String monumento = "";
			monumento = lector.readLine();

			while (monumento != null) {
				System.out.println(monumento);
				monumento = lector.readLine();
			}

		} catch (FileNotFoundException e) {

			System.out.println(MENSAJE_FILENOTFOUNDEXCEPTION);

		} catch (IOException e) {

			System.out.println(MENSAJE_IOEXCEPTION);

		} finally {

			if (lector != null) {

				try {
					lector.close();
				} catch (IOException e) {
					System.out.println(MENSAJE_IOEXCEPTION);
				}

			}

		}

	}

	/**
	 * M�todo que no recibe nada y no devuelve nada. Permite buscar el monumento en
	 * el fichero secuencial, pidiendo su c�digo por teclado. Muestra por consola si
	 * el monumento se encuentra o no en cada una de las actualizaciones del
	 * fichero.
	 */
	public void buscarMonumento() {

		String linea = "";
		BufferedReader lector = null;

		try {

			lector = new BufferedReader(new FileReader(this.nombreFichero));

			System.out.println("Introduce el c�digo del monumento:");
			String codigoMonumento = input.nextLine();

			linea = lector.readLine();

			boolean hayMonumentos = false;
			boolean hayFechas = false;

			while (linea != null) {

				if (linea.startsWith("Monumento")) {

					String codigo = linea.substring(linea.indexOf(":") + 2, linea.indexOf(","));

					if (codigo.equalsIgnoreCase(codigoMonumento)) {
						System.out.println("-> '" + linea + "'\n");
						hayMonumentos = true;
					}

				} else if (linea.startsWith("FECHA")) {

					hayFechas = true;
					System.out.println(linea);

				} else if (linea.isEmpty()) {

					if (!hayMonumentos && hayFechas) {
						System.out.println("-> MONUMENTO '" + codigoMonumento + "' NO SE ENCUENTRA EN LA LISTA\n");
					} else {
						hayMonumentos = false;
					}

				}

				linea = lector.readLine();

			}

			if (!hayMonumentos && !hayFechas) {

				System.out.println("-> EL FICHERO EST� INVERTIDO, ELIGE LA OPCI�N '9' OTRA VEZ");

			}

		} catch (

		FileNotFoundException e) {

			System.out.println(MENSAJE_FILENOTFOUNDEXCEPTION);

		} catch (IOException e) {

			System.out.println(MENSAJE_IOEXCEPTION);

		} finally {

			if (lector != null) {

				try {
					lector.close();
				} catch (IOException e) {
					System.out.println(MENSAJE_IOEXCEPTION);
				}

			}

		}

	}

	/**
	 * M�todo que no recibe nada y no devuelve nada. Devuelve el n�mero de
	 * monumentos de la ciudad introducida por teclado en la �ltima actualizaci�n
	 * del fichero (no se tienen en cuenta todas las actualizaciones). Hace uso del
	 * m�todo {@code ArrayList<String> buscarUltimaListaMonumentos()}
	 */
	public void contarMonumentosCiudad() {

		ArrayList<String> listaMonumentosUltimaActualizacion = buscarUltimaListaMonumentos();

		if (listaMonumentosUltimaActualizacion != null && listaMonumentosUltimaActualizacion.size() > 0) {

			System.out.println(AVISO);

			System.out.println("Introduce la ciudad:");
			String ciudad = input.nextLine();

			int counter = 0;

			String[] informacionMonumento;

			for (String monumento : listaMonumentosUltimaActualizacion) {

				informacionMonumento = monumento.split(",");

				if (informacionMonumento[2].replaceAll(" ciudad: ", "").equalsIgnoreCase(ciudad)) {
					counter++;
				}

			}

			System.out.println("CANTIDAD DE MONUMENTOS EN LA CIUDAD '" + ciudad.toUpperCase() + "': " + counter);

		} else if (listaMonumentosUltimaActualizacion != null) {

			System.out.println(LISTA_SIN_MONUMENTOS);

		}

	}

	/**
	 * M�todo que no recibe nada y no devuelve nada. Muestra por consola los
	 * monumentos que no son de pago de la �ltima actualizaci�n del fichero (no se
	 * tienen en cuenta todas las actualizaciones). Hace uso del m�todo
	 * {@code ArrayList<String> buscarUltimaListaMonumentos()}
	 */
	public void mostrarMonumentosConEntradaGratis() {

		ArrayList<String> listaMonumentosUltimaActualizacion = buscarUltimaListaMonumentos();

		if (listaMonumentosUltimaActualizacion != null && listaMonumentosUltimaActualizacion.size() > 0) {

			System.out.println(AVISO);

			String[] informacionMonumento;
			double precio;
			boolean hayMonumentosGratis = false;

			for (String monumento : listaMonumentosUltimaActualizacion) {

				informacionMonumento = monumento.split(",");
				precio = Double.parseDouble(
						informacionMonumento[3].trim().replaceAll("precio entrada:", "").replaceAll(" �", ""));

				if (precio == 0) {

					System.out.println(monumento);
					hayMonumentosGratis = true;

				}

			}

			if (!hayMonumentosGratis) {
				System.out.println("TODOS LOS MONUMENTOS SON DE PAGO");
			}

		} else if (listaMonumentosUltimaActualizacion != null) {

			System.out.println(LISTA_SIN_MONUMENTOS);

		}

	}

	/**
	 * El m�todo no recibe nada y no devuelve nada. Organiza los monumentos por
	 * c�digo en �rden alfab�tico, y crea una actualizaci�n de la lista. Hace uso
	 * del m�todo {@code void crearFichero()}
	 */
	public void organizarMonumentosPorCodigo() {

		ArrayList<String> listaMonumentosUltimaActualizacion = buscarUltimaListaMonumentos();

		if (listaMonumentosUltimaActualizacion != null && listaMonumentosUltimaActualizacion.size() > 0) {

			Collections.sort(this.listaMonumentos);
			crearFichero();

			System.out.println("MONUMENTOS ORGANIZADOS");

		} else if (listaMonumentosUltimaActualizacion != null) {

			System.out.println("NO HAY MONUMENTOS PARA ORGANIZAR");

		}

	}

	/**
	 * M�todo que elimina un monumento de la �ltima actualizaci�n del fichero. El
	 * c�digo del monumento para eliminar se pide por consola. Hace uso del m�todo
	 * {@code ArrayList<String> buscarUltimaListaMonumentos()}
	 */
	public void eliminarMonumentoDelFichero() {

		PrintWriter escritor = null;

		ArrayList<String> listaMonumentosUltimaActualizacion = buscarUltimaListaMonumentos();

		if (listaMonumentosUltimaActualizacion != null && listaMonumentosUltimaActualizacion.size() > 0) {

			System.out.println(AVISO);

			try {

				System.out.println("Introduce el c�digo del monumento que quieres eliminar:");
				String codigoMonumentoEliminar = input.nextLine();

				if (super.eliminarMonumento(codigoMonumentoEliminar)) {

					escritor = new PrintWriter(new FileWriter(this.nombreFichero, true));

					LocalTime horaActual = LocalTime.now();

					escritor.println("FECHA DE ACTUALIZACI�N: "
							+ LocalDate.now().format(DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM)) + " "
							+ horaActual.getHour() + ":" + horaActual.getMinute());

					for (String monumento : listaMonumentosUltimaActualizacion) {

						String codigo = monumento.substring(monumento.indexOf(":") + 2, monumento.indexOf(","));

						if (!codigo.equals(codigoMonumentoEliminar)) {
							escritor.println(monumento);
						} else {
							escritor.println("-> Monumento '" + codigoMonumentoEliminar + "' eliminado");
							System.out.println("MONUMENTO '" + codigoMonumentoEliminar + "' ELIMINADO");
						}

					}

					escritor.println();

				} else {

					System.out.println("MONUMENTO '" + codigoMonumentoEliminar + "' NO SE ENCUENTRA EN ESTA LISTA");

				}

			} catch (IOException e) {

				System.out.println(MENSAJE_IOEXCEPTION);

			} finally {

				if (escritor != null) {

					escritor.close();

				}

			}

		} else if (listaMonumentosUltimaActualizacion != null) {

			System.out.println(LISTA_SIN_MONUMENTOS);

		}

	}

	/**
	 * M�todo para cambiar la direcci�n de todas las l�neas del fichero de derecha a
	 * izquierda. El m�todo no recibe nada y no devuelve nada.
	 */
	public void cambiarDireccionLineas() {

		String linea = "";

		BufferedReader lector = null;
		PrintWriter escritor = null;
		ArrayList<String> listaTexto = new ArrayList<String>();

		try {

			lector = new BufferedReader(new FileReader(this.nombreFichero));
			linea = lector.readLine();

			while (linea != null) {

				linea = new StringBuilder(linea).reverse().toString();

				System.out.println(linea);

				listaTexto.add(linea);
				linea = lector.readLine();

			}

			escritor = new PrintWriter(new FileWriter(this.nombreFichero, false));

			for (String texto : listaTexto) {

				escritor.println(texto);

			}

		} catch (

		FileNotFoundException e) {

			System.out.println(MENSAJE_FILENOTFOUNDEXCEPTION);

		} catch (IOException e) {

			System.out.println(MENSAJE_IOEXCEPTION);

		} finally {

			if (lector != null) {

				try {
					lector.close();
				} catch (IOException e) {
					System.out.println(MENSAJE_IOEXCEPTION);
				}

			}

			if (escritor != null) {

				escritor.close();

			}

		}

	}

	/**
	 * El m�todo devuelve un ArrayList que contiene la informaci�n de los monumentos
	 * de la �ltima actualizaci�n del fichero. Es un m�todo privado.
	 * 
	 * @return listaMonumentosUltimaActualizacion ArrayList que contiene la
	 *         informaci�n de los monumentos de la �ltima actualizaci�n del fichero,
	 *         null si no existe
	 */
	private ArrayList<String> buscarUltimaListaMonumentos() {

		String linea = "";

		BufferedReader lector = null;
		ArrayList<ArrayList<String>> listaDeListasMonumentos = new ArrayList<ArrayList<String>>();
		ArrayList<String> listaMonumentos = new ArrayList<String>();

		try {

			lector = new BufferedReader(new FileReader(this.nombreFichero));
			linea = lector.readLine();

			boolean hayMonumentos = false;
			boolean hayFechas = false;

			while (linea != null) {

				while (linea.startsWith("Monumento") || (linea.startsWith("->"))) {

					if (linea.startsWith("Monumento")) {
						listaMonumentos.add(linea);
						hayMonumentos = true;
					}

					if (linea.startsWith("FECHA")) {
						hayFechas = true;
					}

					linea = lector.readLine();

					if (linea.isEmpty()) {
						listaDeListasMonumentos.add(listaMonumentos);
						listaMonumentos = new ArrayList<String>();
					}

				}

				linea = lector.readLine();

			}

			if (!hayMonumentos && !hayFechas) {

				System.out.println("-> EL FICHERO EST� INVERTIDO, ELIGE LA OPCI�N '9' OTRA VEZ");

			}

			if (listaDeListasMonumentos.size() > 0) {

				ArrayList<String> listaMonumentosUltimaActualizacion = listaDeListasMonumentos
						.get(listaDeListasMonumentos.size() - 1);

				return listaMonumentosUltimaActualizacion;

			}

		} catch (

		FileNotFoundException e) {

			System.out.println(MENSAJE_FILENOTFOUNDEXCEPTION);

		} catch (IOException e) {

			System.out.println(MENSAJE_IOEXCEPTION);

		} finally {

			if (lector != null) {

				try {
					lector.close();
				} catch (IOException e) {
					System.out.println(MENSAJE_IOEXCEPTION);
				}

			}

		}

		return null;

	}

}
